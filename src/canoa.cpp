#include <iostream>
#include <string>
#include "canoa.hpp"
#include <utility>
using namespace std;

Canoa::Canoa(){
     life=true; 
}
Canoa::~Canoa(){

}

bool Canoa::quebrado(int linha, int coluna){
    pair<int, int> local{linha, coluna};
    return !life;
}
char Canoa::get_caractere(int linha, int coluna){
    pair<int,int> local{linha, coluna};
    //cout<<"C";
    return 'C'; 
}
void Canoa::recebe_tiro(int linha,int coluna){
    pair<int,int>local{linha, coluna};
    life=false;
    cout<<"Canoa Destruída"<<endl;
}
void Canoa::set_cox(int co_x){
    this->co_x = co_x;
}
int Canoa::get_cox(){
    return  co_x;
}
void Canoa::set_coy(int co_y){
    this->co_y = co_y;
}
int Canoa::get_coy(){
    return  co_y;
}