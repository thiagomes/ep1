#include "player.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include "canoa.hpp"
#include "submarino.hpp"
#include "portavioes.hpp"
#include "embarcacao.hpp"
#include <sstream>
using namespace std;

int Player::escolherjogador(){
cout<<"Digite 1 para ser o Jogador um ou Digite 2 para ser o Jogador dois"<<endl;
cin >> a;
return a;
}
string Player::get_map1(){
    return cmap1;
}
void Player::set_map1(char cmap1){
    this->cmap1=cmap1;
}
string Player::get_map2(){
    return cmap2;
}
void Player::set_map2(char cmap2){
    this->cmap2=cmap2;
}
void Player::ler_map(){
    int i = 0; 
    int j = 0; 
    int k = 0; 
    int x;
    int y;
    string tipo;
    string orientacao;
    /*Canoa canoa1;
    Canoa canoa2;
    Submarino submarino1;
    Submarino submarino2;
    Portavioes portavioes1;
    Portavioes portavioes2;*/
//declarar as variaveis dos navios em player.hpp
    ifstream mapa;
    string linha;

    mapa.open("doc/map_1.txt");


    if(mapa.is_open()){
        while( !mapa.eof() ){
        getline(mapa, linha);
            if ( linha[0] == '#'||linha == "" ){
                continue;
            }
            else {
                stringstream ss;
                ss << linha;
                ss >> x;
                ss >> y;
                ss >> tipo;
                ss >> orientacao;
               //vector<pair<int,int>> local={x,y};
                    if (tipo == "canoa"){
                        if(i < 6){
                            //coordenada[i]=local[i];
                            canoa1[i].set_cox(x);
                            canoa1[i].set_coy(y);
                      
                            
                        }
                        else if(i >= 6 && i < 12){
                            //coordenada[i]=local[i];
                            canoa2[i-6].set_cox(x);
                            canoa2[i-6].set_coy(y);                        
                        }
                        
                        i++;
                    }
                    else if(tipo == "submarino"){
                        if(j < 4){
                            //coordenada[j]=local[j];
                            submarino1[j].set_sentido(orientacao);
                            submarino1[j].set_cox(x);
                            submarino1[j].set_coy(y);
                        }
                        else if(j>=4 && j<8){
                            //coordenada[j-4]=local[j-4];
                            submarino2[j-4].set_sentido(orientacao);
                            submarino2[j-4].set_cox(x);
                            submarino2[j-4].set_coy(x);
                        }
                        j++;
                    }
                    else if(tipo == "porta-avioes"){
                        if(k < 2){
                            //coordenada[k]=local[k];
                            portavioes1[k].set_sentido(orientacao);
                           portavioes1[k].set_cox(x);
                           portavioes1[k].set_coy(y);
                        }
                        else if(k>=2 && k<4){
                            //coordenada[k-2]=local[k-2];
                            portavioes2[k-2].set_sentido(orientacao);
                          portavioes2[k-2].set_cox(x);
                          portavioes2[k-2].set_coy(y);
                        }
                        k++;
                    }                              
            }

        }
    }
    else{
        cout << "Nao foi possivel abrir o mapa" << endl;
    }

}
void  Player::compara_map(){
      for(int i=0;i<13;i++){
          for(int j=0;j<13;j++){
              cmap1[i][j]='t';
              cmap[i][j].set_map1(cmap1);
          }
      }
for(int k=0; k<6; k++){
        a = canoa1[k].get_cox();
        b = canoa1[k].get_coy();
        cmap1[a][b] = 'c';
        cmap1[a][b].set_map1(cmap1);
    }
for(int k=0; k<4; k++){
        a = submarino1[k].get_cox();
        b = submarino1[k].get_coy();
        comparacao = submarino1[k].get_sentido();
        cmap1[a][b] = 's';
        cmap1[a][b].set_map1(cmap1);
            if(comparacao == "direita"){
                cmap1[a][b+1] = 's';
                cmap1[a][b].set_map1(cmap1);
            }
            else if(comparacao == "esquerda"){
                cmap1[a][b-1] = 's';
                cmap1[a][b].set_map1(cmap1);
            }
            else if(comparacao == "cima"){
                cmap1[a-1][b] = 's';
                cmap1[a][b].set_map1(cmap1);
            }
            else if(comparacao == "baixo"){
                cmap1[a+1][b] = 's';
                cmap1[a][b].set_map1(cmap1);
            }
    }
for(int k=0; k<2; k++){
        a = portavioes1[k].get_cox();
        b =portavioes1[k].get_coy();
        comparacao = portavioes1[k].get_sentido();
        cmap1[a][b] = 'p';
        cmap1[a][b].set_map1(cmap1);
            if(comparacao == "esquerda"){
                for(int i=1; i<=3; i++){
                    cmap1[a][b-i] = 'p';
                    cmap1[a][b].set_map1(cmap1);
                }
            }
            else if(comparacao == "direita"){
                for(int i=1; i<=3; i++){
                    cmap1[a][b+i] = 'p';
                    cmap1[a][b].set_map1(cmap1);
                }
            }
            else if(comparacao == "cima"){
                for(int i=1; i<=3; i++){
                    cmap1[a-i][b] = 'p';
                    cmap1[a][b].set_map1(cmap1);
                }
            }
            else if(comparacao == "baixo"){
                for(int i=1; i<=3; i++){
                    cmap1[a+i][b] = 'p';
                    cmap1[a][b].set_map1(cmap1);
                }
            }
    }
for(int i=0;i<13;i++){
          for(int j=0;j<13;j++){
              cmap2[i][j]='t';
              cmap2[a][b].set_map2(cmap2);
          }
      }
for(int k=0; k<6; k++){
        a = canoa2[k].get_cox();
        b = canoa2[k].get_coy();
        cmap2[a][b] = 'c';
         cmap2[a][b].set_map2(cmap2);
    }
for(int k=0; k<4; k++){
        a = submarino2[k].get_cox();
        b = submarino2[k].get_coy();
        comparacao = submarino2[k].get_sentido();
        cmap2[a][b] = 's';
         cmap2[a][b].set_map2(cmap2);
            if(comparacao == "direita"){
                cmap2[a][b+1] = 's';
                 cmap2[a][b].set_map2(cmap2);
            }
            else if(comparacao == "esquerda"){
                cmap2[a][b-1] = 's';
                 cmap2[a][b].set_map2(cmap2);
            }
            else if(comparacao == "cima"){
                cmap2[a-1][b] = 's';
                 cmap2[a][b].set_map2(cmap2);
            }
            else if(comparacao == "baixo"){
                cmap2[a+1][b] = 's';
                 cmap2[a][b].set_map2(cmap2);
            }
    }
for(int k=0; k<2; k++){
        a = portavioes2[k].get_cox();
        b =portavioes2[k].get_coy();
        comparacao = portavioes2[k].get_sentido();
        cmap2[a][b] = 'p';
            if(comparacao == "esquerda"){
                for(int i=1; i<=3; i++){
                    cmap2[a][b-i] = 'p';
                     cmap2[a][b].set_map2(cmap2);
                }
            }
            else if(comparacao == "direita"){
                for(int i=1; i<=3; i++){
                    cmap2[a][b+i] = 'p';
                     cmap2[a][b].set_map2(cmap2);
                }
            }
            else if(comparacao == "cima"){
                for(int i=1; i<=3; i++){
                    cmap2[a-i][b] = 'p';
                     cmap2[a][b].set_map2(cmap2);
                }
            }
            else if(comparacao == "baixo"){
                for(int i=1; i<=3; i++){
                    cmap2[a+i][b] = 'p';
                     cmap2[a][b].set_map2(cmap2);
                }
            }
    }
      }


