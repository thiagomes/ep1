#include <iostream>
#include <string>
#include "submarino.hpp"
#include <utility>
#include <vector>
using namespace std;

Submarino::Submarino(){
     life[0]=2;
     life[1]=2; 
}
Submarino::~Submarino(){

}

bool Submarino::quebrado(int linha, int coluna){
    pair<int, int> local{linha, coluna};
    for(int i=0; i<2 ; i++){
        if(coordenada[i]==local){
            if(life[0]==0 && life[1]==0){
                return true;
            }
        }
    }
    
    return false;
}
char Submarino::get_caractere(int linha, int coluna){
    pair<int,int> local{linha, coluna};
        for(int i=0;i<2;i++){
              if(coordenada[i]==local){
                  if(life[i]==1){
                      //cout<<"SM";
                      return 's';
                  }
                  else if(life[i]==0){
                     // cout<<"SD";
                     return 'S';
                  }
              }
    }
    return '0';
}
void Submarino::recebe_tiro(int linha,int coluna){
    pair<int,int>local{linha, coluna};
    for(int i=0;i<2;i++){
        if(coordenada[i]==local){
            life[i]--;
                if(life[i]==1){
                    cout<<"Parte do Submarino quase destruido"<<endl;
                }
                else if(life[i]==0){
                    cout<<"Parte do Submarino destruido"<<endl;
                }
        }
    }
}
string Submarino::get_sentido(){
    return direcao;
}
void Submarino::set_sentido(string direcao){
    this->direcao = direcao;
}
void Submarino::set_cox(int x){
    this->x = x;
}
int Submarino::get_cox(){
    return x;
}
void Submarino::set_coy(int y){
    this->y = y;
}
int Submarino::get_coy(){
    return y;
}