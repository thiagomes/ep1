#include <iostream>
#include <string>
#include "portavioes.hpp"
#include <utility>
#include <vector>
#include <stdlib.h>
#include <time.h>
using namespace std;

Portavioes::Portavioes(){
    life[0]=1;
    life[1]=1;
    life[2]=1;
    life[3]=1;
}

Portavioes::~Portavioes(){

}

bool Portavioes::quebrado(int linha, int coluna){
    pair<int, int> local{linha, coluna};
    for(int i=0;i<4;i++){
        if(coordenada[i]==local){
            if(life[0]==0 && life[1]==0 && life[2]==0 && life[3]==0){
                return true;
            }
        }
    }
    
    return false;
}

void Portavioes::recebe_tiro(int linha,int coluna){
    pair<int,int>local{linha, coluna};
    for(int i=0;i<4;i++){
       if(coordenada[i]==local){
           srand(time(NULL));
             r=rand() % 2;
             if(r==1){
               cout<<"Míssel abatido";
             }
             else{
                 life[i]--;
                 cout<<"Parte do Porta-Aviões Destruído";
             }
       }
    }
}

char Portavioes::get_caractere(int linha, int coluna){
    pair<int,int> local{linha, coluna};
        for(int i=0;i<4;i++){
                  if(life[i]==0){
                      //cout<<"PD";
                      return 'P';
              }
    
}
return '0';
}
string Portavioes::get_sentido(){
    return direcao;
}
void Portavioes::set_sentido(string direcao){
    this->direcao = direcao;
}
void Portavioes::set_cox(int x){
    this->x = x;
}
int Portavioes::get_cox(){
    return x;
}
void Portavioes::set_coy(int y){
    this->y = y;
}
int Portavioes::get_coy(){
    return y;
}