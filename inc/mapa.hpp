#ifndef MAPA_HPP
#define MAPA_HPP
#include <string>
#include <iostream>
#include <vector>
#include <utility>
#include "player.hpp"
#include "canoa.hpp"
#include "submarino.hpp"
#include "portavioes.hpp"
#include "embarcacao.hpp"
using namespace std;
class Mapa:public Player{
private:

    string mapa[13];
    char map1[13][13];
    char map2[13][13]; 
public:
 Player jogador;
    //void lerMapa(string arquivo);
    void mostrarMapa();
    void tiro(int , int);
    pair<int, int> guardatiro(int , int);
     Mapa();
     ~Mapa();
    void recebe_tiro1(int, int);
    void recebe_tiro2(int, int);
};
#endif