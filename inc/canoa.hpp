#ifndef CANOA_HPP
#define CANOA_HPP
#include <iostream>
#include <string>
#include "embarcacao.hpp"
using namespace std;

class Canoa:public Embarcacao{
    private:
    bool life;
    int co_x;
    int co_y;
    public:
    Canoa();
    ~Canoa();
    
         bool quebrado(int linha, int coluna);
           char get_caractere(int linha, int coluna);
               void recebe_tiro(int linha, int coluna);
                        void set_cox(int co_x);
                         void set_coy(int co_y);
                         int get_cox();
                         int get_coy();

};
#endif