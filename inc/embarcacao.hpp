#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP
#include <iostream>
#include <string>
#include <vector>
#include <utility>
using namespace std;

class Embarcacao{
  public:
    
    vector<pair<int, int>> coordenada;
    pair<int, int> sentido;

    Embarcacao();
    ~Embarcacao();
  friend class Player;
  
   
  //  Embarcacao();
   // ~Embarcacao();

    void Set_coordenada(int posicaolinha, int posicaocoluna);
    //string get_sentido(string );
      virtual bool quebrado(int linha, int coluna)=0;
          virtual char get_caractere(int linha, int coluna)=0;
              virtual void recebe_tiro(int linha, int coluna)=0;
             //  /*virtual*/ //string get_sentido();
              // /*virtual*/ void set_sentido(string );
    
};
#endif